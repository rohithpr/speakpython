from SpeakPython.SpeakPython import SpeakPython
from SpeakPython.Result import Result

import math

ans = 0;

def plus(a,b=None):
	global ans;
	if (b==None):
		b = ans;
	ans = a + b
	return ans;

def minus(a,b=None):
	global ans;
	if (b==None):
		b = a;
		a = ans;
	ans = a - b
	return ans;

def divide(a,b=None):
	global ans;
	if (b==None):
		b = a;
		a = ans;

	if b == 0:
		print "You cannot divide by zero silly human. You must be upgraded."
		return;

	ans = float(a) / float(b)
	return ans;

def multiply(a,b=None):
	global ans;
	if (b==None):
		b = ans;
	ans = a * b
	return ans;

def root(a,b=None):
	global ans;
	if (b==None):
		b = ans;

	ans = math.pow(b, (1.0/a));
	return ans;

def pow(a,b=None):
	global ans;
	if (b==None):
		b = a
		a = ans;
	ans = math.pow(a,b);
	return ans;

def clear():
	global ans;
	ans = 0;

def setNum(a):
	global ans;
	ans = a;

sp = SpeakPython("calc.db"); 

inStr = raw_input("text command: ");
while (inStr != "quit"):
	r = sp.matchResult(inStr);

	if r  == None:
		print "Stop spewing nonsense!";
	else:
		rStr = r.getResult();
		exec rStr;
		print "Answer: ", ans;

	inStr = raw_input("text command: ");
