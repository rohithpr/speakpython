(read)? [man] ((pages|page)|$a (pages|page))? ((for|of) $b)?;
@tests
	"man pages for vim" => "man vim"
	"read man 3 page for grep" => "man 3 grep"
@
@results
	0 {'man'}
	$a {'man ' $a}
	0,$b {'man '$b}
	0,$a,$b {'man ' $a ' ' $b}
@

(using|with|via) $b (open|edit) $a;
@tests
	"using vim, open linux.sps" => "vim linux.sps"
@
@results
	a,b {$b' '$a}
@

(open | edit) (file)? $a ((via|using|with) $b)?;
@tests
	"open linux.sps using vim" => "vim linux.sps"
	"edit file linux.sps" => "mimeopen linux.sps"
@
@results
	a {'mimeopen ' $a}
	a,b {$b' '$a}
@

(zip|compress) [everything in]? ((the)? [current (directory|dir)]|($files)+) (as|into) $a [with (subdirs|subdirectories)]?;
@tests
	"zip everything in SpeakPython, examples, SpeakCommands as SpeakCommands.zip" => "zip -r SpeakCommands.zip SpeakPython/examples/SpeakCommands/"
@
@results
	1,a {'zip ' $a ' .'}
	a,k_0 {'zip ' $a ' ' k_0<'/'>($files) '/' }
	0,a,k_0 {'zip -r ' $a ' ' k_0<'/'>($files) '/' }
	2,a,k_0 {'zip -r ' $a ' ' k_0<'/'>($files) '/' }
@

(zip|compress) [everything in]? ((the)? [current (directory|dir)]|($files)+) [with (subdirs|subdirectories)]?;
@tests
	"zip everything in the current directory" => "zip -r SpeechZip.zip ."
@
@results
	k_0 {'zip SpeechZip.zip ' k_0<'/'>($files)}
	0,k_0 {'zip -r SpeechZip.zip ' k_0<'/'>($files)}
	2,k_0 {'zip -r SpeechZip.zip ' k_0<'/'>($files)}
	1 {'zip SpeechZip.zip .'}
	0,1 {'zip -r SpeechZip.zip .'}
	2,1 {'zip -r SpeechZip.zip .'}
@

remove [everything in]? ((the)? file|[folder])? $a;
@results
	a {'rm ' $a}
	0,a {'rm -r ' $a}
	1,a {'rm -r ' $a}
@

(change (directory|dir) | cd) ((to)? (path)?)? ($path)+;
@results
	k_0 {'cd ' k_0<'/'>($path)}
@

create (a)? (new)? (folder|directory|dir) (called)? $a;
@results
	a {'mkdir ' $a}
@

#caseInsensitive() = [((using|with) case insensitivity)|(without case sensitivity)]?;
@tests

@
@results
	0 {'i'}
@

#recursively() = [recursively|(everything in)|(all of)]?;
@tests

@
@results
	0 {'r'}
@


(((look|search) #recursively for) | find) (the (text|string|word) (matching)?)? $a #caseInsensitive #recursively in $b #recursively #caseInsensitive (and (print|show) (the)? (next)? $num lines (after (it)?)?)?;
@tests
	"look for the word grep in blah.txt"
	"search recursively for the string apple using case insensitivity in linux.sps"
	"look for the text grep in linux.sps and show the next 5 lines after it"
@
@results
	a,b {'grep ' $a ' ' $b}

	#recursively,a,b {'grep -' #recursively ' ' $a ' ' $b}
	#caseInsensitive,a,b {'grep -' #caseInsensitive ' ' $a ' ' $b}
	#recursively,#caseInsensitive,a,b {'grep -' #caseInsensitive #recursively ' ' $a ' ' $b}

	num,a,b {'grep -A ' $num ' ' $a ' ' $b}
	num,#recursively,a,b {'grep -A ' $num ' -r ' $a ' ' $b}
	num,#caseInsensitive,a,b {'grep -A ' $num ' -i ' $a ' ' $b}
	num,#recursively,#caseInsensitive,a,b {'grep -A ' $num ' -ir ' $a ' ' $b}
@
