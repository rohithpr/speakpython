#upDown() = ([up]|[down]);
@results
	0 {"up"}
	1 {"down"}
@

#onOff() = ([on]|[off]);
@results
	0 {"on"}
	1 {"off"}
@

#toSetting() = to value;

#appliance() = ([lights|light] | [stove] | [tv|television] | [lamp] | [heating|heat|furnace] | [playstation|(game system)] | [(air conditioning)|ac]);
@results
	0 {"lights"}
	1 {"stove"}
	2 {"television"}
	3 {"lamp"}
	4 {"heat"}
	5 {"ps3"}
	6 {"air"}
@

#feature() = ([volume] | [channel] | [temperature] | [dim ness]);
@results
	0 {"volume"}
	1 {"channel"}
	2 {"temperature"}
	3 {"dimness"}
@

#location() = ([(living room)|(sitting area)] | [bedroom] | [(guest room) | office] | [basement] | [hallway]);
@results
	0 {"living room"}
	1 {"bedroom"}
	2 {"office"}
	3 {"basement"}
	4 {"hallway"}
@

turn the #appliance (#onOff)? (in (the)? #location)?;
@results
	#onOff, #appliance {"Turning the " #appliance " " #onOff " now sir."}
	#onOff, #appliance, #location {"Turning the " #appliance " in the " #location " " #onOff}
	#appliance {"Toggling the " #appliance}
	#appliance, #location {"Toggling the " #location " " #appliance}
@

turn (#onOff)? the #appliance (in (the)? #location)?;
@results
	#onOff, #appliance {"Turning the " #appliance " " #onOff " now sir."}
	#onOff, #appliance, #location {"Turning the " #appliance " in the " #location " " #onOff}
	#appliance {"Toggling the " #appliance}
	#appliance, #location {"Toggling the " #location " " #appliance}
@

change the #feature ((on|of) the #appliance) (#upDown | #toSetting)?;
@results
	#feature,#appliance {"Changing " #feature " on the " #appliance}
	#feature,#appliance,#upDown {"Changing " #feature " on the " #appliance #upDown}
	#feature,#appliance,#toSetting {"Changing " #feature " on the " #appliance #toSetting}
@

turn #upDown the (#feature (on|for))? (the)? #appliance;
@results
	#upDown,#appliance {"Turning " #upDown " the " #appliance}
	#upDown,#feature,#appliance {"Turning " #upDown " the " #feature " on the " #appliance}
@
