from SpeakPython.SpeakPythonRecognizer import SpeakPythonRecognizer;

import RPi.GPIO as GPIO;

import sys;
from random import randint;

leds = {};
leds['red'] = 19;
leds['green'] = 13;
leds['blue'] = 20;
leds['yellow'] = 21;

#these variables are solely used for context (remembering past phrasing)
it = 19;
them = [];

GPIO.setmode(GPIO.BCM);

for led in leds:
	GPIO.setup(leds[led], GPIO.OUT);

stop = False;

def rand(onOff):
	global leds;
	global it;

	idx = leds.keys()[randint(0, len(leds)-1)]; #gen random dict index
	onOff([idx]);
	it = leds[idx]; #remember which one

def handle(led_strs, val):
	global leds;
	global it;
	global them;
	global stop;

	if led_strs == "all":
		#turn all LEDs on/off
		for led in leds:
			GPIO.output(leds[led], val);

		#keep context
		them = leds.values();

	else: #its a list of values
		newThem = [];
		themUsed = False;

		#for all possible lights described, turn them on or off
		for led in led_strs:
			if led == "them" or led == "those":
				for t in them:
					GPIO.output(t, val);
					themUsed = True;
					
			elif led == "it":
				GPIO.output(it, val);
			else:
				GPIO.output(leds[led], val);
				newThem.append(leds[led]);

				#store one done as 'it' (for when there's only 1)
				it = leds[led];

		#keep context
			#replace context when new context arises
			# 'red and blue' means a new 'them' context arose
		if len(led_strs) > 1 and not themUsed:
			them = newThem;
		else: #else we add new leds to the 'them' context
			them.extend(newThem);

def on(led_strs):
	handle(led_strs, 1);

def off(led_strs):
	handle(led_strs, 0);

def quit():
	stop = True;
	off("all");
	sys.exit(0);

def execute(out_str):
	print out_str;
	if not out_str == None:
		exec out_str;
	else:
		print "Couldn't recognize.";

recog = SpeakPythonRecognizer(execute, "LED");
recog.setDebug(1);

#main
try:
	while not stop:
		recog.recognize();
except KeyboardInterrupt:
	stop = True;
	print "Interrupted.";
finally:
	GPIO.cleanup();
