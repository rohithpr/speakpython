import RPi.GPIO as gpio;
import time;

gpio.setmode(gpio.BCM);
gpio.setwarnings(False);

red = 19;
green = 13;
blue = 20;
yellow = 21;

gpio.setup(red, gpio.OUT);
gpio.setup(blue, gpio.OUT);
gpio.setup(green, gpio.OUT);
gpio.setup(yellow, gpio.OUT);

def turnon(led):
	gpio.output(led, 1);
	time.sleep(2);
	gpio.output(led, 0);

try:
	turnon(red);
	turnon(green);
	turnon(blue);
	turnon(yellow);
except KeyboardInterrupt:
	print "Interrupted.";
finally:
	gpio.cleanup();
