#colour() = [red] | [green] | [blue] | [yellow];
@results
	0 {'red'}
	1 {'green'}
	2 {'blue'}
	3 {'yellow'}
	4 {'it'}
	5 {'them'}
@

#colours() = (#colour (and)?)+;
@results
	k_0 {'[' k_0<','>('"' #colour '"') ']'}
@

#onOff() = [on] | [off];
@results
	0 {'on'}
	1 {'off'}
@

#turn() = (turn | put | change);

#turn (the)? #colours (light)? (to)? #onOff;
@results
	#colours, #onOff {#onOff '(' #colours ');'}
@

#colours (light)? (to)? #onOff;
@results
	#colours, #onOff {#onOff '(' #colours ');'}
@

#turn all (of)? (the)? lights #onOff;
@results
	#onOff {#onOff '("all");'}
@

#randomly() = ((at | by)? random | randomly)?;

#randomly #turn (a)? #randomly [light | one] #onOff #randomly;
@results
	#turn, #onOff, 0 {'rand(' #onOff ');'}
@

#turn (a)? #randomly [light | one] #onOff #randomly;
@results
	#turn, #onOff, 0 {'rand(' #onOff ');'}
@

#turn #onOff the #colours (light)?;
@results
	#colours, #onOff {#onOff '(' #colours ');'}
@

[quit | stop | exit];
@results
	0 {'quit();'}
@
