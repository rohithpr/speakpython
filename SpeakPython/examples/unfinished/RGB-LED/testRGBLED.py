import time;
import pigpio;
import sys;

pi = pigpio.pi(); #connect to local pi

#gpio.setmode(gpio.BCM);
#gpio.setwarnings(False);

red = 16;
green = 20;
blue = 21;

def changeColourHex(in_str):
	r = int(in_str[0:2], 16);
	g = int(in_str[2:4], 16);
	b = int(in_str[4:6], 16);

	print str(r) + " " + str(g) + " " + str(b);

	changeColour(r,g,b);

def changeColour(r, g, b):
	global pi, red, green, blue;

	#keep total brightness centered at 1 LED rather than 3 (255)
	s = r + g + b;
	s = float(s);

	pi.set_PWM_dutycycle(red, (r/s)*255);
	pi.set_PWM_dutycycle(green, (g/s)*255);
	pi.set_PWM_dutycycle(blue, (b/s)*255);

#main
try:
#	for r in range(0, 51):
#		for g in range(0, 51):
#			for b in range(0, 51):
#				changeColour(r*5, g*5, b*5);
#				time.sleep(0.1);

#	changeColour(255, 0, 0);
#	time.sleep(3);
#	changeColour(0, 255, 0);
#	time.sleep(3);
#	changeColour(0, 0, 255);
#	time.sleep(3);

	inHex = sys.argv[1];
	changeColourHex(inHex);
	time.sleep(10);
except KeyboardInterrupt:
	print "Qutting.";

finally:
	#turn off PWM (try at least)	
	pi.set_PWM_dutycycle(red, 0);
	pi.set_PWM_dutycycle(green, 0);
	pi.set_PWM_dutycycle(blue, 0);
	pi.stop();
