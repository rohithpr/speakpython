" vim syntax file
" Created By: Eric Matthews
" Language: SpeechPhraseSpecification (.sps)
" Maintainer: Eric Matthews
" Latest Revision: November 17th 2014

if exists("b:current_syntax")
	finish
endif

" Keywords
syn keyword fields tests results
syn keyword default default

" Matches
syn match regexString '\v/.+/r'
syn match comment '//.*$'
syn match alias '\v#[a-zA-Z0-9]+'
syn match variable '\$[a-zA-Z][a-zA-Z0-9]*'
syn match labels '[\t ]*(#)?[a-zA-Z0-9]([\t ]*,[\t ]*(\#)?[a-zA-Z0-9]+)*[\t ]*(>={.*$)'
syn match exp '\v[a-zA-Z0-9]+'
syn match or '\v\|'
syn match semi ';'
syn match question '?'
syn match quoteString '((".+")|(\'.+\'))'

" Regions
syn region resultBlock start='{' end='}' contains=quoteString,result,variable,alias
syn region resultFieldBlock start='@results' end='@' contains=quoteString,fields,comment,alias,variable,labels,exp,or,resultBlock
syn region testFieldBlock start='@tests' end='@' contains=quoteString,comment
syn region globalOptionBlock start='@globalOptions' end='@' contains=comment,regexString
syn region optionBlock start='@options' end='@' contains=comment,default,exp,regexString

let b:current_syntax = "sps"

hi def link comment	Comment
hi def link alias	Statement
hi def link variable	Identifier
hi def link exp		Constant
hi def link result	Statement
hi def link question	PreProc
hi def link labels	PreProc
hi def link semi	Statement
hi def link quoteString	PreProc
hi def link regexString PreProc
